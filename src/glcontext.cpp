#include <libGL.hpp>
#include <algorithm>

namespace gl
{
	std::map<const char*, GLExtension> GLContext::s_extensionMap;

	GLContext::GLContext(unsigned int majVersion, unsigned int minVersion) :
		m_majVersion(majVersion),
		m_minVersion(minVersion)
	{
		gl::mapInit<std::map<const char*, GLExtension>>(GLContext::s_extensionMap)
			("GL_ARB_arrays_of_arrays", ARRAYS_OF_ARRAYS)
			("GL_ARB_depth_clamp", DEPTH_CLAMP)
			("GL_ARB_multisample", MULTISAMPLE)
			("WGL_ARB_render_texture", RENDERTEXTURE)
			("GL_ARB_texture_non_power_of_two", TEXTURE_NON_PO2)
			("GL_EXT_texture3D", TEXTURE_3D)
			("GL_EXT_texture_cube_map", CUBE_MAP);
	}

	GLContext::~GLContext()
	{
		for (auto* window : m_windows)
			window->destroy();

		glfwTerminate();
	}

	Window* GLContext::createContext(unsigned int width, unsigned int height, const std::string& title)
	{
		bool isCreatingContext = (m_windows.size() == 0);

		if (isCreatingContext)
			if (!glfwInit())
			{
		    FATAL("FATAL: We couldn't create a GLFW context!")
			}

		Window* window = new Window(width, height, title);
		m_windows.push_back(window);

		if (isCreatingContext)
		{
			glewInit();
			verifyGLVersion();
		}

		return window;
	}

	void GLContext::verifyGLVersion()
	{
		unsigned int supportedMajVersion, supportedMinVersion;

		glGetIntegerv(GL_MAJOR_VERSION, (int*) &supportedMajVersion);
		glGetIntegerv(GL_MINOR_VERSION, (int*) &supportedMinVersion);

		if ((!(m_majVersion == 0 && m_minVersion == 0)) && (supportedMajVersion < m_majVersion ||
			(supportedMajVersion == m_majVersion && supportedMinVersion < m_minVersion)))
		{
			FATAL("FATAL: The requested OpenGL version is not available on this system!")
		}

		unsigned int numExtensions;
		glGetIntegerv(GL_NUM_EXTENSIONS, (int*) &numExtensions);
		std::cout << "Found " << numExtensions << " supported extensions!" << std::endl;

		for (unsigned int i = 0; i < numExtensions; i++)
		{
			std::string extensionName = std::string((const char*) glGetStringi(GL_EXTENSIONS, i));

			if (s_extensionMap.count(extensionName.c_str()))
				m_supportedExtensions.insert(s_extensionMap[extensionName.c_str()]);
			else
				std::cout << "Extension supported but not registered: " << extensionName << std::endl;
		}

		if (m_supportedExtensions.size() > 0)
			std::cout << "Yaya!" << std::endl;
	}

	bool GLContext::isExtensionSupported(GLExtension extension) const
	{
		return std::find(m_supportedExtensions.begin(), m_supportedExtensions.end(), extension) != m_supportedExtensions.end();
	}
};
