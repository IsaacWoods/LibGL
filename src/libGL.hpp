#ifndef LIB_GL_H
#define LIB_GL_H

#include <string>
#include <vector>
#include <set>
#include <map>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <util.hpp>

// For conciseness - have just a single definition for Windows systems
#if !defined(_WIN32) && (defined(__WIN32__) || defined(WIN32) || defined(__MINGW32__))
	#define WINDOWS
#endif

#define FATAL(message) std::cerr << message << std::endl; \
	*((unsigned int*) 0) = 0xDEADBEEF;

namespace gl
{
	enum GLExtension
	{
		ARRAYS_OF_ARRAYS,
		DEPTH_CLAMP,
		MULTISAMPLE,
		RENDERTEXTURE,
		TEXTURE_NON_PO2,
		TEXTURE_3D,
		CUBE_MAP
	};

	class Window;

	class GLContext
	{
		public:
			GLContext(unsigned int majVersion = 0, unsigned int minVersion = 0);
			~GLContext();

			bool isExtensionSupported(GLExtension extension) const;
			Window* createContext(unsigned int width = 800, unsigned int height = 600, const std::string& title = std::string("LibGL Window"));
		private:
			unsigned int m_majVersion, m_minVersion;
			std::set<GLExtension> m_supportedExtensions;
			std::vector<Window*> m_windows;
			void initContext();
			void verifyGLVersion();

			static std::map<const char*, GLExtension> s_extensionMap;
	};

	/*
	 * 'Window' is the representation of a physical OpenGL window.
	 * These need to be bound to a 'GLContext' to be created.
	 */
	class Window
	{
		public:
			virtual ~Window();

			bool shouldClose() const;
			void update();
			void destroy();

			friend class GLContext;
		protected:
			unsigned int m_width, m_height;
			std::string m_title;
		private:
			GLFWwindow* m_window;
			Window(unsigned int width, unsigned int height, const std::string& title);
	};
};

#endif // LIB_GL_H
