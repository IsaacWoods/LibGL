#include <libGL.hpp>

int main()
{
	gl::GLContext context(3, 3);
	gl::Window* window = context.createContext(800, 600, "LibGL Window!");

	while (!window->shouldClose())
		window->update();

	return 0;
}
