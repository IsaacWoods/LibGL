System  Design
==============
LibGL provides an abstraction layer on top of the standard, provided OpenGL functions and introduces a high-level,
object-orinted approach to OpenGL. It replaces the repetitive, time-consuming tasks such as shader program creation or texture
binding with an elegant, designed for access from C++.

It aims to simplify:
* Window creation & managment
* Shader program managment
* Texture managment

All LibGL functions are contained within the `gl` namespace and are accessed using an object-oriented ideology.

Window Creation and Managment
-----------------------------
Windows can be managed using the `gl::Window` class. It has the constructor: ` gl::Window(unsigned int width = 800, unsigned int
height = 600, const std::string& title = std::string("LibGL Window"))`
