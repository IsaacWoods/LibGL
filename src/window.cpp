#include <libGL.hpp>

namespace gl
{
	Window::Window(unsigned int width, unsigned int height, const std::string& title) :
		m_width(width),
		m_height(height),
		m_title(title)
	{
		// glfwSetErrorCallback(errorCallback);
	  m_window = glfwCreateWindow(m_width, m_height, m_title.c_str(), NULL, NULL);

	  if (!m_window)
	  {
	    glfwTerminate();
	    FATAL("FATAL: We couldn't create a GLFW window handle!")
	  }

	  glfwMakeContextCurrent(m_window);
	}

	Window::~Window()
	{
		destroy();
	}

	bool Window::shouldClose() const
	{
		return glfwWindowShouldClose(m_window);
	}

	void Window::update()
	{
		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}

	void Window::destroy()
	{
		glfwDestroyWindow(m_window);
	}
};
