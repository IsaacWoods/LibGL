#ifndef UTIL_H
#define UTIL_H

namespace gl
{
  template<typename T> struct MapInitHelper
  {
      T& data;
      MapInitHelper(T& d) : data(d) {}

      MapInitHelper& operator() (typename T::key_type const& key, typename T::mapped_type const& value)
      {
          data[key] = value;
          return *this;
      }
  };

  template<typename T> MapInitHelper<T> mapInit(T& item)
  {
      return MapInitHelper<T>(item);
  }
};

#endif // UTIL_H
